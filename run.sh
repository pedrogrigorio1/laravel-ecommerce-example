#!/bin/bash
composer install
php artisan key:generate
php artisan ecommerce:install --force
npm run dev
php artisan serve --host 0.0.0.0